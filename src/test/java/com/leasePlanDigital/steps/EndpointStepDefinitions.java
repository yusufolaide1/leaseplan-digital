package com.leasePlanDigital.steps;

//import org.apache.hc.core5.http.ContentType;
//
//import static io.restassured.matcher.RestAssuredMatchers.*;
//import static org.hamcrest.Matchers.*;
//import org.testng.annotations.Test;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

//import net.thucydides.core.annotations.Step;
//import static io.restassured.matcher.RestAssuredMatchers.given;

@SuppressWarnings("deprecation")
public class EndpointStepDefinitions {
	
	String baseUrl = "http://xkcd.com/";
	RequestSpecification request;
	private  static  Response response;
	private  static  String  jsonString;

		@Given("GET request API")
		public void Test() {
			//request = new RequestSpecBuilder()
					//.setBaseUri(baseUrl)
					//.build();
			RestAssured.baseURI  =  baseUrl;
			request  =  RestAssured.given();
			request.header("Content-Type",  "application/json");
		}
		
		@When("I perform GET request operation on the url")
		public void Test2() {
			response = request.get("info.0.json");
		}

		@And("I convert my response string and get json obj")
		public void Test3(){
			String res = getJsonPath(response, "num");
			Assert.assertEquals("2431", res);
		}
		
		@Then("Status_code equals {int}")
		public void Test3(int arg) {
			//throw new io.cucumber.java.PendingException();
			Assert.assertEquals(arg, response.getStatusCode());
		}
		
		public String getJsonPath(Response response, String key) {
			String resp = response.asString();
			JsonPath js = new JsonPath(resp);
			return js.get(key).toString();
		}

		@When("I enter comicID as {int}")
		public void getByComicId(int args){
			response = request.get(args+"/info.0.json");
		}

		@Then("I should see title return as {string}")
	public void getTitleFromJson(String args){
			String title = getJsonPath(response, "title");
			Assert.assertEquals(args, title);
		}

	@When("I enter an aphabet comic ID {string}")
	public void getUsingEmptyComicId(String args){
		response = request.get(args+"/info.0.json");
	}

	@Then("Status_code should return error {int}")
	public void emptyCodeError(int arg) {
		//throw new io.cucumber.java.PendingException();
		Assert.assertEquals(arg, response.getStatusCode());
	}

	@When("I enter an invalid comic ID {int}")
	public void getUsingInvalidDigit(int args){
		response = request.get(args+"/info.0.json");
	}

	@Then("status_code error should return {int}")
	public void statusCodeOnInvalidNum(int arg) {
		//throw new io.cucumber.java.PendingException();
		Assert.assertEquals(arg, response.getStatusCode());
	}

}



