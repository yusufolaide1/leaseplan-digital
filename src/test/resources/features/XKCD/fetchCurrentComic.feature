Feature: Fetch Data
@FetchCurrentComics
Scenario: User Should be able to Fetch Current Comics and Metadata Automatically
	Given GET request API
	When I perform GET request operation on the url
	And I convert my response string and get json obj
	Then Status_code equals 200

#Scenario: FetchComicID
    Given GET request API
    When I enter comicID as 614
    Then I should see title return as "Woodpecker"

#Scenario: Fetch Empty ComicID
    Given GET request API
    When I enter an aphabet comic ID "q"
    Then Status_code should return error 404

#Scenario: Fetch Invalid ComicID
    Given GET request API
    When I enter an invalid comic ID 1234567
    Then status_code error should return 404
 
