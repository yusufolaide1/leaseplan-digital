#HOW TO INSTALL

Using the SERENITY BDD BOOK as reference found in https://serenity-bdd.github.io/theserenitybook/latest/cucumber.html

#Pre-Requisites

1. Java 1.8
2. IDE (IntelliJ / Eclispe) 

#Steps to Install
1. Open command Line 
2. cd to the directory project folder
3. enter "mvn archetype:generate -Dfilter=serenity"
4. Select the first option from the results by entering 1
5. Enter 21
6. Enter you artifact, group ID and package name (optional)
7. Enter y

Launch the project in any of the IDE by importing from the local directory project folder

#HOW TO WRITE TEST 
1. Create your .feature file 
2. Create your Acceptance java class (run configuration)
3. Create your steps definition file
4. The features to be tested which includes the Features, Scenario, Run tag and the Gherkin priciple Given-When-Then are written in the .feature file
5. While the steps definition are written in the steps file


#ENDPOINT INTERACTION [XKCD API]
1. The base url was used for the Given method
2. The ./path{comicID}/info.0.json was used to fetch the corresponding comicID




